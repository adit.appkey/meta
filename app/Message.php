<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = ['id'];
    protected $fillable = [
        'message_id',
        'whatsapp_business_account_id',
        'contact_id',
        'phone_number',
        'text_body',
        'message_type',
        'user_type',
        'status',
        'created_at',
        'updated_at'
    ];

    public function WhatsappBusiness(): BelongsTo
    {
        return $this->belongsTo(WhatsappBusinessAccount::class, 'user_id');
    }

    public function Contact(): BelongsTo
    {
        return $this->belongsTo(Contact::class, 'user_id');
    }
}
