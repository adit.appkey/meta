<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $guarded = ['id'];
    protected $fillable = [
        'name',
        'phone_number',
        'whatsapp_business_account_id',
        'created_at',
        'updated_at'
    ];

    public function WhatsappBusiness(): BelongsTo
    {
        return $this->belongsTo(WhatsappBusinessAccount::class, 'whatsapp_business_account_id');
    }
}
