<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JsonWebhook extends Model
{
    protected $guarded = ['id'];
    protected $fillable = ['id', 'data'];
}
