<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    public function getMessage(request $request)
    {
        $messages = Message::where('phone_number', $request['phone'])->get();

        return response()->json([
            'status' => 'successfully',
            'data' => $messages,
            'code' => 200
        ], 200);
    }

    public function sendMessage(request $request)
    {
        $data = $request->all();
        $message = Message::create([
            'message_id' => $data['message_id'],
            'whatsapp_business_account_id' => 1,
            'phone_number' => $data['phone'],
            'text_body' => $data['text_body'],
            'message_type' => $data['message_type'],
            'user_type' => $data['user_type'],
            'status' => $data['status']
        ]);

        return response()->json([
            'message' => 'success', 
            'status' => 200,
            'data' => $message
        ], 200);
    }
}
