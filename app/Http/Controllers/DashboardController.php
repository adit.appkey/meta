<?php

namespace App\Http\Controllers;

use App\JsonWebhook;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $data = JsonWebhook::orderBy('id', 'desc')->get();
        return view('dashboard.index', compact('data'));
    }
}
