<?php

namespace App\Http\Controllers;

use App\Message;
use App\JsonWebhook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CallbackController extends Controller
{
    // MyTokenMeta
    public function index(Request $request)
    {
        $payload = $request->getContent();

        // if (json_decode($payload)) {
        $data = json_decode($request->getContent(), true);

        if (isset($data['entry'][0]['changes'][0]['value']['statuses'])) {
            return $request['hub_challenge'];
            // return "entar dulu cek statusnya";
        }elseif(isset($data['entry'][0]['changes'][0]['value']['messages'][0])) {
            $this->_handleMessage($data['entry'][0]['changes'][0]['value']['messages'][0]);
        }else{
            return $request['hub_challenge'];
        };

        $json = JsonWebhook::create(['data' => $payload]);
    
        return $request['hub_challenge'];
        // } else {
        //     return response()->json(['error' => 'Invalid JSON'], 400);
        // };
    }

    public function show($id)
    {
        try {
            $json = JsonWebhook::find($id);
            
            $json->data = str_replace('\n', '', $json->data);
            $json->data = json_decode($json->data);
    
            return response()->json($json->data);
    
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function destroy($id)
    {
        try{ 
        
            $json = JsonWebhook::find($id);

        if (!$json) {
            // Objek tidak ditemukan, mungkin hendak mengarahkan ke halaman lain atau menampilkan pesan kesalahan.
            return Redirect::route('dashboard')->with('error', 'Data not found');
        }
    
        // Objek ditemukan, maka lakukan penghapusan.
        $json->delete();

            return Redirect::route('dashboard.index')->with('success', 'Data has been deleted successfully'); 
    
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
    }

    public function _handleContact($data)
    {

    }

    public function _handleMessage($data)
    {
        Message::create([
            'message_id' => $data['id'],
            'whatsapp_business_account_id' => 1,
            'phone_number' => $data['from'],
            'text_body' => $data['text']['body'],
            'message_type' => $data['type'],
            'user_type' => 'receive',
            'status' => 'delivered'
        ]);
    }
}
