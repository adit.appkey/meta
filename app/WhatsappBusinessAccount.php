<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatsappBusinessAccount extends Model
{
    protected $guarded = ['id'];
    protected $fillable = [
        'waba_id',
        'phone_number',
        'phone_number_id',
        'user_id',
        'created_at',
        'updated_at'
    ];

    public function User(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
