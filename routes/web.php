<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MessageController;
use \App\Http\Controllers\CallbackController;
use App\Http\Controllers\DashboardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashboardController::class, 'index'])->name('dashboard.index');

Route::post('webhook', [CallbackController::class, 'index'])->name('callback');
Route::get('webhook/{id}', [CallbackController::class, 'show'])->name('webhook.show');
Route::get('webhook/delete/{id}', [CallbackController::class, 'destroy'])->name('webhook.delete');

Route::get('/chats', function(){
    return view('chat.index');
})->name('chat.index');

Route::get('API/message', [MessageController::class, 'getMessage'])->name('getMessage');
Route::get('API/send-message', [MessageController::class, 'sendMessage'])->name('sendMessage');