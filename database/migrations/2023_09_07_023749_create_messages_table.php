<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->string('phone_number');
            $table->string('message_id');
            $table->foreignId('whatsapp_business_account_id')->nullable()->constrained('whatsapp_business_accounts');
            $table->foreignId('contact_id')->nullable()->constrained('contacts');
            $table->string('text_body');
            $table->string('message_type');
            $table->enum('user_type', ['receive', 'send'])->nullable();
            $table->enum('status', ['sent', 'delivered', 'read'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
